// Repitition Control Structures

// While Loop
/*
- A while loop takes in an expression/condition
- If the ccondition evaluates to be true, the statements inside the ccode will be executed
-Syntax:
	while (expression/condition) {
		statement
	}

*/

// let count = 5;

// // While the value of count is not equal to zero, it will run
//  while (count !== 0) {

//  	// The current value of count is printed out
//  	console.log("While: " + count);

//  	// Decreases the value of count by 1 after every iteration
//  	count--;
//  }


// Do While Loop
/*
- A do-while loop works a lot like the while loop but it guaranteest that the ccode will be executed atleast once.
-Syntax:
	do {
		statement
	} while (expression/condition)

*/

 // let number = Number(prompt("Give me a number: "));

 // do {
 // 	// the current value of number is printed out
 // 	console.log("Do While: " + number);

 // 	// increases the value of number by 1 after every iteration
 // 	// number = number + 1
 // 	number += 1;

 // 	// providing a number of 10 or greater will run the code blocck once but the loop will stop there
 // } while (number < 10);


 // For Loop
/*
- A for loop is more flexible than while and do-while loops.
- It consists of three parts
	1. The initial value which is the starting value. The count will start at the initial value.
	2. The expression/condition that will determine if the loop will run one more time. While this is true, the loop will run.
	3. The finalExpression which determines how the loop will run (increment/decrement)
- Syntax:
	for (initial; expression/condition; finalExpression) {
		statement
	}

*/

 for (let count = 0; count <= 20; count++) {
 	console.log(count)
 }

let myString = "arjay";
console.log(myString.length);
// Characters in string may be counted using the .length property
// Individual characters of a string may be acccessed using its index number
// The first character in a string has an index of "0". The next is one and so on.

 // This loop prints the individual letters of the myString variable
 for (let x=0; x < myString.length; x++) {
 	// The current value of myString is printed out using its index value
 	console.log(myString[x]);
 }

 let myName = "akosimrpogi";
 for (let i = 0; i < myName.length; i++) {
 	if (
 		myName[i].toLowerCase() == "a" ||
 		myName[i].toLowerCase() == "e" ||
 		myName[i].toLowerCase() == "i" ||
 		myName[i].toLowerCase() == "o" ||
 		myName[i].toLowerCase() == "u" 
 		) {
 		console.log();
 	} else {
 		console.log(myName[i]);
 	}
 }

 // Continue and Break
 /*
 - The "continue" statement allows the code to go to the next iteration of a loop without finishing the execution of a statements in a code block.

 */

 for (let count = 0; count <= 20; count++) {
 	if (count % 2 === 0) {
 		continue;
 	}

 	console.log("Continue and Break: " + count)

 	if (count > 10){
 		break;
 	}
 }

 let name = "jakedlexter";

 for (let i = 0; i < name.length; i++) {
 	console.log(name[i]);

 	if (name[i].toLowerCase() === "a") {
 		console.log("Continue to the next iteration");
 		continue;
 	}

 	if (name[i] == "d") {
 		break;
 	}
 }