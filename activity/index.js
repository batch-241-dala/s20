;
let number = Number(prompt("Give me a number: "));
console.log("The number you provided is: " + number);

for (number; number > 50; number--) {
	
	if (number % 10 === 0) {	
		
		console.log("The number is divisible by 10. Skipping the number.")
		continue;
	}

	if (number % 10 === 5) {
		
		
		console.log(number)
		continue;
	} 

	if (number < 50) {
		console.log("The current value is at " + number + ". Terminating the loop.")
		break;
	}
}

let string = "supercalifragilisticexpialidocious";
let result = "";
 for (let i = 0; i < string.length; i++) {
 	if (
 		string[i].toLowerCase() == "a" ||
 		string[i].toLowerCase() == "e" ||
 		string[i].toLowerCase() == "i" ||
 		string[i].toLowerCase() == "o" ||
 		string[i].toLowerCase() == "u" 
 		) {
 		console.log();
 	} else {
 		result += string[i];
 	}
 }
 console.log(string);
 console.log(result);